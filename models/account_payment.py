from odoo import fields, models


class AccountPayment(models.Model):
	_inherit = "account.payment"

	to_check = fields.Boolean(string='À Vérifier', default=False, tracking=True)

