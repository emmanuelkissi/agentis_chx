from odoo import fields, models
from odoo.exceptions import ValidationError

class AccountMoveInherit(models.Model):
	_inherit = "account.move"

	# Modification du nom de la facture et prise en compte de la reference
	def action_post(self):
		print("Le action_post", self)
		if self.payment_id:
			self.payment_id.action_post()
		else:
			self._post(soft=False)
		if self.ref:
			
			name = self.name
			name = name.split('/')
			name[-1] = str(self.ref)
			
			# Verification de l'existance du nom 
			account_id = self.env['account.move'].search([('name', '=', '/'.join(name))])
			print("Son if", account_id, name)
			if account_id and account_id != self:
				raise ValidationError("Cette réference existe déja. Veuillez le modifié pour poursuive la confirmation")

			self.name = '/'.join(name)
				
		return False

class AccountMoveLineInherit(models.Model):
	_inherit = "account.move.line"

	to_check = fields.Boolean(string='À Vérifier', default=False, tracking=True)